
    function calculate() {
    const xValues = document.getElementById('xValues').value.split(',').map(Number);
    const yValues = document.getElementById('yValues').value.split(',').map(Number);


    if (xValues.length !== yValues.length) {
    alert("n(x)!=n(y)");
    return;
}

    // mean
    const meanX = xValues.reduce((a, b) => a + b) / xValues.length;
    const meanY = yValues.reduce((a, b) => a + b) / yValues.length;

    //  sum
    let sumXY = 0, sumXX = 0, sumYY = 0;
    for (let i = 0; i < xValues.length; i++) {
    sumXY += (xValues[i] - meanX) * (yValues[i] - meanY);
    sumXX += (xValues[i] - meanX) * (xValues[i] - meanX);
    sumYY += (yValues[i] - meanY) * (yValues[i] - meanY);
}


    const B = sumXY / sumXX;
    const A = meanY - B * meanX;

    // Y Pr
    const yPred = xValues.map(x => A + B * x);

    // R
    const SS_tot = yValues.reduce((acc, val) => acc + Math.pow(val - meanY, 2), 0);
    const SS_res = yValues.reduce((acc, val, i) => acc + Math.pow(val - yPred[i], 2), 0);
    const R2 = 1 - (SS_res / SS_tot);

    // F
    const n = xValues.length;
    const k = 1; // Number of predictors
    const F = (R2 / (1 - R2)) * ((n - k - 1) / k);

    // TR
    const r = sumXY / Math.sqrt(sumXX * sumYY);


    document.getElementById('results').innerHTML = `
                <p>A: ${A.toFixed(4)}</p>
                <p>B: ${B.toFixed(4)}</p>
                <p>R: ${R2.toFixed(4)}</p>
                <p>F: ${F.toFixed(4)}</p>
                <p>TR: ${r.toFixed(4)}</p>
            `;
}
